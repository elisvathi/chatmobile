//@flow
import React, { Component } from 'react'
import { View, Text,StatusBar,Platform, KeyboardAvoidingView,  StyleSheet, TouchableOpacity } from 'react-native'

import ConversationsContainer from '../containers/ConversationsContainer';
import UsersContainer from '../containers/UsersContainer';
import {ChatNav} from './Chat/ChatNavigator'
import {NavigationContainer} from './Navigators/AppNavigator';
// import Expo from 'expo';
class HomePage extends Component {
    componentWillMount(){
       
    }
    state = {  }
    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={{flex: 1,paddingTop:Platform.OS==='ios'?0:StatusBar.currentHeight}}>
            <NavigationContainer />
            </KeyboardAvoidingView>
        );
    }
}



export default HomePage;