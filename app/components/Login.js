import React, { Component } from 'react'
import LoginForm from './LoginForm';
import { View, Text, StyleSheet , Image, KeyboardAvoidingView} from 'react-native'
import RegisterForm from './RegisterForm';
class Login extends Component {
    constructor(props){
        super(props);
        this.state ={loginFormShown:true}
        this.swap = this.swap.bind(this);
    }
    
    swap(){
       
        let shown;
        if(!this.state){shown = true}else{
            shown = this.state.loginFormShown;
        }
        // let shown = this.state.loginFormShown;
        this.setState({loginFormShown:!shown})
    }
    
    renderForm(){
        if(this.state.loginFormShown){
            return  <LoginForm loginAction ={this.props.loginAction} swap = {this.swap}/>

        }else{
            return <RegisterForm register= {this.props.register} swap = {this.swap}/>
        }
    }
    render() {
        
        return (
            <KeyboardAvoidingView behavior="padding" style = {styles.container}>
                <View style = {styles.topPart}>
                    <Image style ={styles.logo} source = {require('../resources/logo.png')}/>
                    <Text style={styles.title}>Welcome to the newest Chat App</Text>
                </View>
                <View style={styles.formContainer}>
                    {/* <LoginForm loginAction ={this.props.loginAction}/> */}
                    {this.renderForm()}
                </View>
            </KeyboardAvoidingView>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#3498db',
        flex: 1
    },
    topPart:{
        
        alignItems:"center",
        flexGrow: 1,
        justifyContent: 'center'
       
    },
    formContainer:{
       

    },
    logo:{
        width:100,
        height: 100
    },
    title:{
        color: 'white',
        marginTop : 10,
        width: 140,
        textAlign: 'center',
        opacity: 0.6

    }
})


export default Login;