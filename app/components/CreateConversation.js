import React, { Component } from 'react'
import { View, Text, ListView, TextInput, Button, StyleSheet } from 'react-native'
import {bindActionCreators} from 'redux';
import ActionCreators from '../actions';
import {connect} from 'react-redux';

class CreateConversation extends Component {
    state = {  selectedUsers:[], unSelectedUsers:[], message:""}
    componentWillMount(){
        this.setState({unSelectedUsers: this.props.users})
    }
    addItem(user){
        let usr = this.state.unSelectedUsers;
        let id = usr.indexOf(user);
        let item = usr[id];
        usr.splice(id,1);
        let sel = this.state.selectedUsers;
        sel.push(item);
        this.setState({selectedUsers: sel, unSelectedUsers: usr});
    }
    removeItem(user){
        let usr = this.state.selectedUsers;
        let id = usr.indexOf(user);
        let item = usr[id];
        usr.splice(id,1);
        let sel = this.state.unSelectedUsers;
        sel.push(item);
        this.setState({selectedUsers: usr, unSelectedUsers: sel});
    
    }
    getIds(){
        let retVal = [];
        this.state.selectedUsers.map((x)=>{retVal.push(x.id)});
        return retVal;
    }
    renderUnselected(user){
        return <View key={user.id} style ={styles.elementContainer}>
            <Text style = {styles.input}>{user.name}</Text><Button style= {[styles.button, styles.AddButton]} onPress = {()=>{this.addItem(user)}} title="Add"></Button>
        </View>
    }
    renderSelected(user){
        return <View key={user.id} style ={styles.elementContainer}>
            <Text style = {styles.input}>{user.name}</Text><Button style= {[styles.button, styles.RemoveButton]} onPress={()=>{this.removeItem(user)}} title = "Remove"></Button>
        </View>
    }
    handleSend(){
        if(this.state.selectedUsers.length >0 && this.state.message !=""){
            this.props.createConversation(this.getIds(), this.state.message);
        }
    }
    render() {
        return (

            <View style = {styles.mainContainer}>
                
                <View style = {styles.ListContainer}>
                    {this.state.unSelectedUsers.map((x)=>{return this.renderUnselected(x)})}
                </View>
                <View style = {styles.ListContainer}>
                    {this.state.selectedUsers.map((x)=>{return this.renderSelected(x)})}
                </View>
                
                <View style={[styles.messageContainer, styles.elementContainer]}>
                    <TextInput style={styles.input} placeholder="type message here" value ={this.state.message} onChangeText={(t)=>{this.setState({message: t})}}/>
                        <Button style={styles.button} title = "Create" onPress={()=>{this.handleSend()}}></Button>
                </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex: 1,
        justifyContent: 'center'
    },
    ListContainer:{
        flex: 1,
        
    },
    elementContainer:{
        flex: 1,
        flexDirection: 'row'
    },
    button:{
        flex: 0.3
    },
    input: {
        flex: 0.7
    },
    AddButton:{
        backgroundColor: 'blue'
    },
    RemoveButton:{
        backgroundColor: 'red'
    },
    messageContainer:{
        flex: 0.2
    }

})

const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators(ActionCreators, dispatch);
}
const mapStateToProps = (state)=>{
    return {
        user: state.currentUser.data,
        users: state.users.data,
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(CreateConversation);