import { StackNavigator, StackRouter, TabRouter, DrawerNavigator, TabNavigator } from 'react-navigation';
import HomePage from '../HomePage';
import ChatComponent from '../Chat/ChatComponent'
import SettingsContainer from '../../containers/SettingsContainer'
import ConversationsContainer from '../../containers/ConversationsContainer'
import UsersContainer from '../../containers/UsersContainer'
import DiscoverContainer from '../../containers/DiscoverContainer'
import React, { Component } from 'react'
import UserProfile from '../UserProfile';
import MainMenu from '../MainMenu'
import CreateConversation from '../CreateConversation';



export const HomeTabs = TabNavigator({
    Discover: { screen: DiscoverContainer, title: "Discover" },
    Contacts: { screen: UsersContainer, title: "Contacts" },
    Conversations: { screen: ConversationsContainer, title: "Chats" }
});
export const MainMasterDetail = DrawerNavigator({
    Home: { screen: HomeTabs },
    Settings: { screen: MainMenu }
})
export const MainNavigator = StackNavigator({
    Main: { screen: MainMasterDetail },
    Chat: { screen: ChatComponent },
    Profile: { screen: UserProfile },
    Settings: { screen: SettingsContainer },
    CreateConversation: {screen: CreateConversation}
})

export class NavigationContainer extends Component {
    
        static stackRouter = StackRouter({
            Home: { screen: MainMasterDetail, path: '' },
            Profile: { screen: UserProfile, path: 'profile/:id' },
            Settings: { screen: SettingsContainer, path: 'settings' },
            Conversation: { screen: ChatComponent, path: 'chat/:id' }
        });
        static tabRouter = TabRouter({
            Discover: { screen: DiscoverContainer, path: 'discover' },
            Contacts: { screen: UsersContainer, path: 'contacts' },
            Conversations: { screen: ConversationsContainer, path: 'conversations' }
        }, { initialRouteName: "Conversations" })
        render() {
            return (
                <MainNavigator />
            );
        }
    }