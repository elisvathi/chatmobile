import React, { Component } from 'react'
import { View, Text, TextInput, TouchableOpacity , StyleSheet, StatusBar} from 'react-native'

class RegisterForm extends Component {
    state = { fullName: '', username: '', password:'', confifrmPassword:'' }
    submitRegister(){
       

        
        this.props.register(this.state.fullName, this.state.username, this.state.password)
    }
    render() {
        return (
            <View style = {styles.container}>
            <StatusBar barStyle='light-content'/>
            <TextInput 
            placeholder="username or email"
            placeholderTextColor = "rgba(255,255,255,0.3)"
            underlineColorAndroid= 'rgba(0,0,0,0)'
            returnKeyType = 'next'
            autoCapitalize = 'none'
            autoCorrect = {false}
            onSubmitEditing ={()=>this.fullNameInput.focus()}
            value = {this.state.username}
            onChangeText = {(text)=>{this.setState({username: text})}}
            style ={styles.input}/>
            <TextInput 
            placeholder="full name"
            placeholderTextColor = "rgba(255,255,255,0.3)"
            underlineColorAndroid= 'rgba(0,0,0,0)'
            returnKeyType = 'next'
            autoCapitalize = 'none'
            autoCorrect = {false}
            onSubmitEditing ={()=>this.passwordInput.focus()}
            ref = {(input)=>this.fullNameInput = input}
            value = {this.state.fullName}
            onChangeText = {(text)=>{this.setState({fullName: text})}}
            style ={styles.input}/>
            <TextInput 
            placeholder="password" 
            value = {this.state.password}
            onChangeText = {(text)=>this.setState({password: text})}
            underlineColorAndroid= 'rgba(0,0,0,0)'
            placeholderTextColor = "rgba(255,255,255,0.3)"
            returnKeyType = 'next'
            onSubmitEditing = {()=>this.passwordConfirmInput.focus()}
            secureTextEntry
            ref ={(input)=>this.passwordInput=input}
            style ={styles.input}/>
            <TextInput 
            placeholder="confirm password" 
            value = {this.state.confifrmPassword}
            onChangeText = {(text)=>this.setState({confifrmPassword: text})}
            underlineColorAndroid= 'rgba(0,0,0,0)'
            placeholderTextColor = "rgba(255,255,255,0.3)"
            returnKeyType = 'go'
            secureTextEntry
            ref ={(input)=>this.passwordConfirmInput=input}
            style ={styles.input}/>
            <TouchableOpacity style = {styles.buttonContainer} onPress = {()=>this.submitRegister()}>
                <Text style={styles.buttonText}>REGISTER</Text>
            </TouchableOpacity>
            <Text style={styles.helperText}>Already have an account?</Text>
            <TouchableOpacity style = {styles.buttonContainer} onPress = {()=>this.props.swap()}>
                <Text style={styles.buttonText}>LOG IN</Text>
            </TouchableOpacity>
        </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container:{
            padding: 20
        },
        input:{
            height: 40,
            backgroundColor: 'rgba(255,255,255,0.2)',
            marginBottom: 10,
            paddingHorizontal : 10,
            color: '#FFF',
          
        },
        buttonContainer:{
            backgroundColor :"#2980b9",
            paddingVertical: 15
        },
        buttonText:{
            textAlign: 'center',
            color: '#FFFFFF',
            fontWeight: '700'
        },
        helperText:{
            textAlign: 'center',
            color: "#FFFFFF"
        }
    }
)

export default RegisterForm;