import {StackNavigator} from 'react-navigation';
import ChatComponent from './ChatComponent'
import ConversationsContainer from '../../containers/ConversationsContainer'
export const ChatNav = StackNavigator({
    Main: {screen: ConversationsContainer},
    Chat: {screen: ChatComponent}
})