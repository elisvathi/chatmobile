import React, { Component } from 'react'
import { View, Text, KeyboardAvoidingView , ScrollView, FlatList,StyleSheet} from 'react-native'
import {List, ListItem} from 'native-base';
import MessageComponent from './MessageComponent';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ActionCreators from '../../actions'
import MessageForm from './MessageForm'
class ChatComponent extends Component {
    constructor(props){
        super(props);
        this.sendMessage = this.sendMessage.bind(this);
    }
    state = {  }
    componentWillMount() {
       
       
        const {params} = this.props.navigation.state;
        
       
        let id = params.conversation.conversation.id;
        if(!this.props.messages.fetched.hasOwnProperty(id) || this.props.messages.fetched[id]==false){
        this.props.fetchMessages(id);}
    }
    componenentDidMount(){
        this.scrollToBottom();
    }
    componentDidUpdate(prevProps, prevState) {
        this.scrollToBottom();
    }
    renderMessages(){
        const {params} = this.props.navigation.state
       
        const {conversation} = params;
        const {messages, user, conversations} = this.props;
        const {data, fetched, fetching, errors} = messages;
        const{id} = conversation.conversation;
        const areFetching = fetching.hasOwnProperty(id)?fetching[id]:false;
        const areFetched = fetched.hasOwnProperty(id)?fetched[id]:false;
        const error = errors.hasOwnProperty(id)?errors[id]:null;
        if(!data.hasOwnProperty(id)){
            return <View><Text>Loading Conversation...</Text></View>
        }else{
            if(areFetching && !areFetched){
                return <View><Text>Loadding Messages ...</Text></View>
            }
            else if(error!=null){
                return <View><Text>Error loading messages..</Text></View>
            }else{
                return <View>
                    {data[id].map(
                        (x)=>{
                            return <MessageComponent user={this.findUser(x)} key={x.id} isCurrentUser={this.isByCurrentUser(x)} message = {x}/>
                        }
                    )}
                </View>
            }
        }
    }
    findUser(message){
        const id = message.user_id;
        const conv_id = message.conversation_id;
        let conv = this.props.conversations.find((x)=>{return x.conversation.id == conv_id})
        
        let users = conv.users

       
        var retVal = users.find((element)=>{return element.id==id})
       
        if(!retVal){return this.props.user}
        return retVal;
    }
    isByCurrentUser(message){
       
        return message.user_id == this.props.user.id;
    }
    sendMessage(message){
        const {params} = this.props.navigation.state
        
         const {conversation} = params;
        this.props.sendMessage(conversation.conversation.id, message);
    }
    scrollToBottom(){
        // const resp =this.scroll.getScrollResponder();
        this.scroll.scrollToEnd({animated:true});
    }
    render() {
        return (
            <KeyboardAvoidingView behavior = 'padding' style = {styles.container}>
                <ScrollView style = {styles.messageArea} ref={(element)=>{this.scroll=element}} horizontal = {false}> 
                    {this.renderMessages()}
                </ScrollView>
                <View style={styles.formArea}>
                    <MessageForm send = {this.sendMessage} />
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    messageArea:{
        flex:0.9,
        backgroundColor: '#FFFFFF'
    },
    formArea:{
        flex:0.1
    }
})

const mapStatetoProps = (state)=>{
    return {
        messages: state.messages,
        user: state.currentUser.data,
        conversations: state.conversations.data
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStatetoProps, mapDispatchToProps)(ChatComponent);