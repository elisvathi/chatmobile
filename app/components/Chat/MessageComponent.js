import React, { Component } from 'react'
// import {ListItem , Text} from 'native-base'
import { StyleSheet, View, Text } from 'react-native'
import moment from 'moment';
class MessageComponent extends Component {
    state = {}
    renderMessage() {
        if (this.props.isCurrentUser) {

            return this.renderSelfMessage()
        } else {

            return this.renderUserMessage()
        }
    }
    renderSelfMessage() {

        return (
            <View style = {styles.selfMessageBox}>
        <View style={styles.selfMessageContainer}>
        <Text >{this.senderName()}</Text>
            <Text style={styles.selfMessage}>{this.props.message.message}</Text>
            <Text style={styles.selfTime}>{moment(this.props.message.created_at).fromNow()}</Text>
            </View></View>)
    }
    senderName(){
        var a= this.props.user.name.split(' ');
        return a[0];
    }
    renderUserMessage() {
        return(
            <View style={styles.otherMessageBox}>
         <View style={styles.otherMessageContainer}>
         <Text >{this.senderName()}</Text>
             <Text style={styles.otherMessage}>{this.props.message.message}</Text>
             <Text style={styles.selfTime}>{moment(this.props.message.created_at).fromNow()}</Text>
             </View></View>)
    }
    render() {
        return this.renderMessage();
    }
}
const styles = StyleSheet.create({
    selfMessage: {
        textAlign: 'left',
        color: '#FFFFFF',
        fontSize: 12
    },
    selfTime:{
        color: 'rgba(255,255,255,0.4)',
        fontSize: 9
    },
    otherMessage: {
        textAlign: 'right',
        color: '#34495e',
        fontSize: 12
    },
    messageContainer: {
        // height: '200'
    },
    selfMessageBox:{
       
        alignItems: 'flex-end'
    },
    otherMessageBox:{
  
        alignItems: 'flex-start'
    },
    selfMessageContainer: {
        backgroundColor: "#3498db",
       
        padding: 5,

        margin: 2,
        marginRight: 20,
        borderRadius: 5,
        maxWidth: 300,
        
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 10 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        
    },
    otherMessageContainer: {
        backgroundColor: '#bdc3c7',
       
        padding: 6,
        margin: 2,
       
        marginLeft: 20,
        maxWidth:300,
        
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 10 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
    }
})

export default MessageComponent;