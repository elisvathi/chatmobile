import React, { Component } from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'

class MessageForm extends Component {
    state = { message:'' }
    submitMessage(){
        if(this.state.message!='')
        {this.props.send(this.state.message)}
        this.setState({message:''})
    }
    render() {
        return (
            <View style ={styles.container}>
                <TextInput style={styles.input}
                placeholder="type message here.."
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor = 'rgba(255,255,255,0.3)'
                returnKeyType = 'go'
                value = {this.state.message}
                onChangeText = {(text)=>{this.setState({message: text})}}
                onSubmitEditing = {()=>this.submitMessage()}
                />
                <TouchableOpacity style={styles.sendButton} onPress = {()=>this.submitMessage()}>
                    <Text style={styles.buttonText}>Send</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection: 'row'
    },
    input:{
        flex:4,
        backgroundColor: 'rgba(255,255,255,0.3)'
    },
    sendButton:{
        flex:1,
        backgroundColor: '#16a085'
    },
    buttonText:{
        color: '#FFFFFF',
        textAlign: 'center'
    }
})


export default MessageForm;