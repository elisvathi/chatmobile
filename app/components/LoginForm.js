import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Text, TouchableOpacity, StatusBar } from 'react-native'
class LoginForm extends Component {
    state = { username: '', password: '' }
    submitLogin() {

        // this.props.loginAction(this.state.username, this.state.password);
        this.props.loginAction('elisvathi@outlook.com', 'vathielis');
    }
    render() {
        
        return (
            <View style={styles.container}>
                <StatusBar barStyle='light-content' />
                <TextInput
                    placeholder="username or email"
                    placeholderTextColor="rgba(255,255,255,0.3)"
                    underlineColorAndroid='rgba(0,0,0,0)'
                    returnKeyType='next'
                    autoCapitalize='none'
                    autoCorrect={false}
                    onSubmitEditing={() => this.passwordInput.focus()}
                    value={this.state.username}
                    onChangeText={(text) => { this.setState({ username: text }) }}
                    style={styles.input} />
                <TextInput
                    placeholder="password"
                    value={this.state.password}
                    onChangeText={(text) => this.setState({ password: text })}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholderTextColor="rgba(255,255,255,0.3)"
                    returnKeyType='go'
                    secureTextEntry
                    ref={(input) => this.passwordInput = input}
                    style={styles.input} />
                <TouchableOpacity style={styles.buttonContainer} onPress={() => this.submitLogin()}>
                    <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
                <Text style = {styles.helperText}>Dont have an account?</Text>
                <TouchableOpacity style={styles.buttonContainer} onPress={() => this.props.swap()}>
                    <Text style={styles.buttonText}>REGISTER</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create(
    {
        container: {
            padding: 20
        },
        input: {
            height: 40,
            backgroundColor: 'rgba(255,255,255,0.2)',
            marginBottom: 10,
            paddingHorizontal: 10,
            color: '#FFF',

        },
        buttonContainer: {
            backgroundColor: "#2980b9",
            paddingVertical: 15
        },
        buttonText: {
            textAlign: 'center',
            color: '#FFFFFF',
            fontWeight: '700'
        },
        helperText:{
            textAlign: 'center',
            color: "#FFFFFF"
        }
    }
)

export default LoginForm;