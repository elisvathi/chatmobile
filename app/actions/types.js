export function createPromiseType(type){
    return {
        default: type,
        pending: type + "_PENDING",
        success: type + "_FULFILLED",
        rejected: type + "_REJECTED",
        
        
    }
}
function explicitActionType(type){return type + "_EXPLICIT"}

// function insertionTypes(type)

export function createResourceType(type){
    return {
        default: type,
        get: createPromiseType("FETCH_"+ type),
        edit: createPromiseType("EDIT_"+type),
        delete: createPromiseType("DELETE_"+ type),
        expEdit: explicitActionType("EDIT_" + type),
        expDelete: explicitActionType("DELETE_" + type),
        expAdd: explicitActionType("INSERT_" + type),
        expReset: explicitActionType("RESET_" + type)
    }
}
export function indexedType(type, index){
    return type+ '_' + index;
}
export function indexedPromiseTYpe(type, index){
    return createPromiseType(indexedType(type, index));
}

export const messagesResourceTYpes = createResourceType(messagesSimpleType);
export const conversationsResourceTYpes = createResourceType(conversationsSimpleType);

export const fetchConversationsTypes = createPromiseType("FETCH_CONVERSATIONS");
export const fetchMessagesTypes = createPromiseType("FETCH_MESSAGES");
export const fetchUsersTypes = createPromiseType("FETCH_USERS");
export const fetchCurrentUserTypes = createPromiseType("FETCH_CURRENT_USER");
export const logInTypes = createPromiseType("TRY_LOG_IN");
export const LogInTrue = "LOG_IN_TRUE";
export const LogInFalse = "LOG_IN_FALSE";
export const LoggingIn = "LOG_IN_PENDING";
export const sendMessageTypes = createPromiseType("SEND_MESSAGE")
export const createConversationTypes = createPromiseType("CREATE_CONVERSATION");

export const messagesSimpleType = "MESSAGES";
export const conversationsSimpleType = "CONVERSATIONS";
export const currentuserSimpleType = "CURRENT_USER";
export const usersSimpleTime = "USERS"