import * as helpers from '../lib/axiosRest';
import * as types from './types';
export function fetchConversations()
{
    return  {type: types.fetchConversationsTypes.default ,
        payload: helpers.get(helpers.routes.conversations)
    }
}

export function fetchMessages(convIndex){
    return {
        type :types.indexedType(types.fetchMessagesTypes.default, convIndex),
        payload : helpers.getIndexed(convIndex, helpers.routes.messages)
    }
}

export function fetchUsers(...args){
    return {
        type: types.fetchUsersTypes.default,
        payload: helpers.get(helpers.routes.users)
    }
}

export function fetchCurrentUser(){
    return {type: types.fetchCurrentUserTypes.default,
    payload: helpers.get(helpers.routes.user)}
}

export function sendMessage(convId, text){
    return{
        type: types.sendMessageTypes.default,
        payload: helpers.postIndexed({message: text},convId, helpers.routes.messages)
    }
}
export function createConversation(userIds, messageText){
    return {
        type: types.createConversationTypes.default,
        payload: helpers.post({userIds: userIds, messageText: messageText}, helpers.routes.conversations)
    }
}





export function addMessage(message){
   
    let id = message.conversation_id;
    return{
        type: types.createResourceType(types.indexedType(types.messagesSimpleType, id)).expAdd,
        payload: message
    }

}


export function updateLastMessage(message){
    return (dispatch, getState)=>{
        const convs = getState().conversations.data;
        console.log(convs);
        console.log(message);
        let ind = convs.find((x)=>{return x.conversation.id == message.conversation_id});
        console.log(ind);
        let item = ind;
        item.lastMessage = message;
        dispatch(updateConversation(item));
    }
}

export function updateConversation(conversation){
    let id = conversation.id;
    return {
        type: types.createResourceType(types.conversationsSimpleType).expEdit,
        payload: conversation
    }
}