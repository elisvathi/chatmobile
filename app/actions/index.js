import * as restActions from './restActions';
import * as userServices from './userServices';
import * as AuthServices from '../lib/AuthService';
export default ActionCreators  = Object.assign({}, restActions, userServices, AuthServices);
