import * as types from './types';
import * as apiHelpers from '../lib/axiosRest';

export function logOut(){
    return {
        type: types.logInTypes.clear,
        payload:{}
    }
}

export function logInTrue(){
    return {
        type: types.LogInTrue
    }
}

export function loggingIn(){
    return{
        type: types.LoggingIn
    }
}

export function logInFalse(r=null){
    return {
        type: types.LogInFalse,
        payload: r
    }
}