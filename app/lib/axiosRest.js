
import axios from 'axios';
import * as auth from './AuthService';
export const routes = {
    root:  "http://sampleproject.app.192.168.1.198.xip.io",
    conversations : 'api/conversations',
    messages: 'api/messages',
    users: 'api/users',
    user: 'api/user',
    getToken: 'oauth/token',
    tokenRefresh: 'oauth/token/refresh',
    register: 'api/createuser'
}

function getFullUrl(...elements){
    return joinUrl(routes.root , ...elements);
}
function joinUrl(...routes){
    return routes.join('/')
}

export function get(...root){
   

    return axios.get(getFullUrl(...root));
}
export function getIndexed(index, ...root){
   
   return get(...root, index);
}

export function post(data, ...root){
   
    return axios.post(getFullUrl(...root), data);
}
export function postIndexed(data, index, ...route){
    return axios.post(getFullUrl(...route, index), data);
}

export function update(data, ...route){
    return axios.update(getFullUrl(...route), data)
}
export function updateIndexed(data, index, ...route){
    return axios.update(getFullUrl(...route), data)
}

export function remove(...route){
    return axios.delete(getFullUrl(...route))
}
