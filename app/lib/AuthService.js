
import * as helpers from './axiosRest';
import axios from 'axios';
import * as userServices from '../actions/userServices';
import {listener} from './eventListener';
export var authData = {};

export function authorize(username, password){
    let data = {
        password: password,
        username: username,
        client_id: 4,
        grant_type: "password",
        client_secret: "HerZeygknsFpT4krjvQYo1kyF39fjwvXY97jLZrV",
        scope: '*'
    }
    return (dispatch, getState)=>{
        dispatch(userServices.loggingIn())
    helpers.post(data, helpers.routes.getToken).then(
        r=>{authData = r.data; 
          
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + authData.access_token;
            listener.setAuthToken("Bearer " + authData.access_token);
            dispatch(userServices.logInTrue());
        }
    ).catch(
        r=>{authData = {}, dispatch(userServices.logInFalse(r))}
    )}
}
export function refreshToken(){
    let data = {
        client_id: 4,
        client_secret: "HerZeygknsFpT4krjvQYo1kyF39fjwvXY97jLZrV",
        scope: '*',
        grant_type: 'refresh_token',
        refresh_token: authData.refresh_token
    }
    return (dispatch, getstate)=>{
        helpers.post(data, helpers.routes.tokenRefresh).then(
            r=>{authData = r.data;
            axios.defaults.headers.common['Authorization']='Bearer ' + authData.access_token;
            listener.setAuthToken("Bearer " + authData.access_token);
            dispatch(userServices.logInTrue())
        }
        ).catch(
            r=>{authData = {}, dispatch(userServices.logInFalse())}
        )
    }
}
export function logOut(){

    return(dispatch,getState)=>{
    authData = {};
    axios.defaults.headers.common['Authorization'] = '';
    listener.setAuthToken('');
    dispatch(userServices.logInFalse());}
}

export function register(name, email, password){
    data = {
        client_id: 4,
        client_secret: "HerZeygknsFpT4krjvQYo1kyF39fjwvXY97jLZrV",
        name: name,
        email: email,
        password: password
    }
    return (dispatch, getState)=>{
    helpers.post(data, helpers.routes.register).then(
        (r)=>{
            
            dispatch(authorize(email, password));
        }
    ).catch((r)=>{console.log(r)})
    }
}