import * as io from 'socket.io-client';
import { routes } from './axiosRest.js';
class Listener {
    socket: SocketIOClient.Emitter
    namespace: string = "App.Events"
    channelNames: string[];
    privateChannels: string[];
    events: string[];
    url: string;
    opts: Options
    constructor(url: string) {
        this.url = url;
        this.channelNames = [];
        this.privateChannels = [];
        this.events = [];
        this.opts = {
            appId: "MobApp",
			key: "87f625be81533b7d1472d0d7228d08f6",
            auth: {
                headers: {
                    Authorization: ""
                }
            }
        }
        this.socket = io.connect(url);
        this.socket.on('reconnect', () => this.reapplyChannels());
        this.socket.on('disconnect', () => this.reconnect());

    }
    setAuthToken(token) {
        this.opts.auth.headers.Authorization = token;
        console.log(this.opts);
    }
    channel(name): Listener {
        if (this.channelNames.indexOf(name) == -1) {
            this.channelNames.push(name);
        }
        this.socket.emit('subscribe', { channel: name })
        return this;
    }
    private(name): Listener {
        if (this.privateChannels.indexOf(name) == -1) {
            this.privateChannels.push(name);
        }
        this.socket.emit('subscribe', { channel: 'private-' + name, auth: this.opts.auth , appId: this.opts.appId, key:this.opts.key})
        return this;
    }
    unsubscribe(name): Listener {
        var removed = this.channelNames.splice(this.channelNames.indexOf(name), 1);
        this.socket.emit('unsubscribe', { channel: removed[0] });
        return this;
    }
    usubscribeAll(): Listener {
        this.channelNames.forEach(element => {
            this.unsubscribe(element)
        });
        return this;
    }
    bind(event, callback): Listener {
        if (this.events.indexOf(event) == -1) {
            this.listen(event)
        }
        let index = this.events.indexOf(event);
        this.socket.addEventListener(this.formatEvent(this.events[index]), callback);
        return this;
    }
    unbind(event, callback): Listener {
        let index = this.events.indexOf(event);
        if (index != -1) {
            this.socket.removeEventListener(this.formatEvent(this.events[index]), callback);
        }
        return this;
    }
    unbindAll(event) {
        if (this.events.indexOf(event) != -1) {
            var _ev = this.formatEvent(event);
            var list = this.socket.listeners(_ev);
            for (var i = list.length - 1; i >= 0; i++) {
                this.socket.removeListener(_ev, list[i]);
            }
        }
    }
    unbindAllEvents(): Listener {
        this.events.forEach(element => {
            this.unbindAll(element)
        });
        return this;
    }
    listen(event, callback = null): Listener {
        if (this.events.indexOf(event) != -1) {
            this.events.push(event);
        }
        var _ev = this.formatEvent(event);
        this.socket.on(_ev, (a, b) => { callback(b) });
        return this;
    }
    formatEvent(event): string {
        var data = this.namespace.split('.')
        data.push(event);
        var retVal = data.join('\\');
        return retVal;
    }
    reapplyChannels() {
        if (this.channelNames) {
            this.channelNames.forEach(element => {
                this.channel(element);
            });
        }
        if(this.privateChannels){
            this.privateChannels.forEach(el=>{this.private(el)})
        }
    }
    reconnect() {
        this.socket = io.connect(this.url);

    }
}

interface Options {
    appId: string,
    key: string
    auth: Auth
}
interface Auth {
    headers: AuthHeaders
}
interface AuthHeaders {
    Authorization: string
}

export const listener = new Listener("http://sampleproject.app.192.168.1.198.xip.io" + ":6001"); 