import {createPromiseType , createResourceType} from '../actions/types';


export function createReducer(initialState, handlers){
    return function reducer(state=initialState, action){
        if(handlers.hasOwnProperty(action.type)){
            return handlers[action.type](state, action);
        }else{
            return state;
        }
    }
}
export function indexedReducer(initialState, handlers, connector){
    return function reducer(state = initialState, action){
        if(connector(state,handlers, action)){
                return connector(state, handlers, action);
        }else{
            return state;
        }
    }
}
export function simpleIndexedReducer(type){
    return indexedReducer({data:{}}, {
        [type](state, action, index){
            let newData = state.data;
            newData[index] = action.payload;
            return {...state, data: newData}
        }
    }, simpleConnector)
}
export function promiseIndexedReducer(type){
    const types = createPromiseType(type);
    const types2 = createResourceType(type);
    return indexedReducer({
        data:{}, fetched:{}, fetching:{}, errors: {}
    }, {
        [types2.get.pending](state, action, index){
            let newFetched = state.fetched;
            let newFetching = state.fetching;
            let newErrors = state.errors;
            newFetched[index] = false;
            newFetching[index] = true;
            newErrors[index] = null;
            return {...state, fetched: newFetched, fetching: newFetching, errors: newErrors}
        },
        [types2.get.rejected](state, action, index){
            let newFetched = state.fetched;
            let newFetching = state.fetching;
            let newErrors = state.errors;
            newErrors[index] = action.payload.response;
            newFetched[index] = false;
            newFetching[index] = false;
            return {...state, fetched: newFetched, fetching: newFetching, errors: newErrors}
        },
        [types2.get.success](state, action, index){
            let newFetched = {...state.fetched};
            let newFetching = {...state.fetching};
            let newErrors = {...state.errors};
            let newData = {...state.data};
            newData[index] = action.payload.data;
            newFetched[index] = true;
            newFetching[index] = false;
            newErrors[index] = null;
            return {...state, fetched: newFetched, fetching: newFetching, errors: newErrors, data: newData}
        },
        [types2.expAdd](state, action , index){
            let data2 = {...state.data};
            if(data2.hasOwnProperty(index)){
                data2[index].push(action.payload)
            }
            return {...state, data: data2}
        },
        [types2.expEdit](state, action, index){
            let data2 = {...state.data};
            if(data2.hasOwnProperty(index)){
                let elem = data2[index].find((a)=>{return a.id==action.payload.id})
                data2[index][data2[index].indexOf(elem)] = action.payload
            }
            return {...state, data: data2}
        },
        [types2.expDelete](state, action, index){
            let data2 = {...state.data};
            if(data2.hasOwnProperty(index)){
                data2[index].splice(action.payload,1)
            }
            return {...state, data: data2}
        },
        [types2.expReset](state, action, index){
            let data2 = {...state.data};
            let errors = {...state.errors}
            if(data2.hasOwnProperty(index)){
                data2[index] = [];
            }
            return {...state, data: data2}
        }
    }, promiseConnector)
}
function simpleChecker(handlers, type){
    let str = type.split('_');
    str.splice(str.length-2,2);
    let newString = str.join();
    return handlers.hasOwnProperty(newString);
}
function simpleParser(type){
    let str = type.split('_');
    let index = parseInt(str[str.length-1]);
    str.splice(str.length-2,1);
   
    let newString = str.join('_');
    return {fullType: newString, index: index}
}
function promiseParser(type){
    let str = type.split('_');
    let promisePart = str[str.length-1];
    let index = str[str.length-2];
    str.splice(str.length-2,2);
    let cleanType = str.join('_');
    let fullType = cleanType + '_' + promisePart;
    return {fullType: fullType, index: index}
}
function simpleConnector(state, handlers, action){
    return baseConnector(simpleParser, state, handlers, action);
}
function promiseConnector(state, handlers, action){
    return baseConnector(promiseParser, state, handlers, action);
}
function baseConnector(parser, state, handlers, action){
    let pars = parser(action.type);
    let newString = pars.fullType;
    let index = pars.index;
    if( handlers.hasOwnProperty(newString)){
        return handlers[newString](state, action, index)
    }else{
        return null;
    }
}
export function promiseReducer(type){
    const types = createPromiseType(type);
    const types2 = createResourceType(type);
    return createReducer({
        data:null, fetched: false, fetching: false, error: null
    }, {
        [types2.get.pending](state, action){
            return {...state, fetched:false, fetching:true, error:null}
        },
        [types2.get.success](state, action){
            return {...state, fetched:true, data: action.payload.data, error: null, fetching: false}
        },
        [types2.get.rejected](state, action){
            return {...state, fetched:false, fetching: false, error: action.payload.response}
        },
        [types2.expReset](state, action){
            return {...state, fetched: false, fetching: false, error: null, data: null}
        },
        [types2.expAdd](state, action){
            let data2 = {...state.data}
            if(Array.isArray(data2)){
            data2.push(action.payload)}else{
                let a = [];
                a.push(data2);
                data2 = a;
            }
            return {...state, data: data2}
        },
        [types2.expEdit](state,action){
            let data2;
            if(Array.isArray(state.data)){
           data2 = [...state.data]
           
            if(Array.isArray(data2)){
            let elem = data2.find((a)=>{return a.id == action.payload.id})
            if(elem){
                data2[data2.indexOf(elem)] = action.payload;
            }}}
            else{data2 = action.payload}
            
            return {...state, data: data2}
        },
        [types2.expDelete](state, action){}
    })
}
