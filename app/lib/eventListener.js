"use strict";
exports.__esModule = true;
var io = require("socket.io-client");
var Listener = /** @class */ (function () {
    function Listener(url) {
        var _this = this;
        this.namespace = "App.Events";
        this.url = url;
        this.channelNames = [];
        this.privateChannels = [];
        this.events = [];
        this.opts = {
            appId: "MobApp",
            key: "87f625be81533b7d1472d0d7228d08f6",
            auth: {
                headers: {
                    Authorization: ""
                }
            }
        };
        this.socket = io.connect(url);
        this.socket.on('reconnect', function () { return _this.reapplyChannels(); });
        this.socket.on('disconnect', function () { return _this.reconnect(); });
    }
    Listener.prototype.setAuthToken = function (token) {
        this.opts.auth.headers.Authorization = token;
        console.log(this.opts);
    };
    Listener.prototype.channel = function (name) {
        if (this.channelNames.indexOf(name) == -1) {
            this.channelNames.push(name);
        }
        this.socket.emit('subscribe', { channel: name });
        return this;
    };
    Listener.prototype.private = function (name) {
        if (this.privateChannels.indexOf(name) == -1) {
            this.privateChannels.push(name);
        }
        this.socket.emit('subscribe', { channel: 'private-' + name, auth: this.opts.auth, appId: this.opts.appId, key: this.opts.key });
        return this;
    };
    Listener.prototype.unsubscribe = function (name) {
        var removed = this.channelNames.splice(this.channelNames.indexOf(name), 1);
        this.socket.emit('unsubscribe', { channel: removed[0] });
        return this;
    };
    Listener.prototype.usubscribeAll = function () {
        var _this = this;
        this.channelNames.forEach(function (element) {
            _this.unsubscribe(element);
        });
        return this;
    };
    Listener.prototype.bind = function (event, callback) {
        if (this.events.indexOf(event) == -1) {
            this.listen(event);
        }
        var index = this.events.indexOf(event);
        this.socket.addEventListener(this.formatEvent(this.events[index]), callback);
        return this;
    };
    Listener.prototype.unbind = function (event, callback) {
        var index = this.events.indexOf(event);
        if (index != -1) {
            this.socket.removeEventListener(this.formatEvent(this.events[index]), callback);
        }
        return this;
    };
    Listener.prototype.unbindAll = function (event) {
        if (this.events.indexOf(event) != -1) {
            var _ev = this.formatEvent(event);
            var list = this.socket.listeners(_ev);
            for (var i = list.length - 1; i >= 0; i++) {
                this.socket.removeListener(_ev, list[i]);
            }
        }
    };
    Listener.prototype.unbindAllEvents = function () {
        var _this = this;
        this.events.forEach(function (element) {
            _this.unbindAll(element);
        });
        return this;
    };
    Listener.prototype.listen = function (event, callback) {
        if (callback === void 0) { callback = null; }
        if (this.events.indexOf(event) != -1) {
            this.events.push(event);
        }
        var _ev = this.formatEvent(event);
        this.socket.on(_ev, function (a, b) { callback(b); });
        return this;
    };
    Listener.prototype.formatEvent = function (event) {
        var data = this.namespace.split('.');
        data.push(event);
        var retVal = data.join('\\');
        return retVal;
    };
    Listener.prototype.reapplyChannels = function () {
        var _this = this;
        if (this.channelNames) {
            this.channelNames.forEach(function (element) {
                _this.channel(element);
            });
        }
        if (this.privateChannels) {
            this.privateChannels.forEach(function (el) { _this.private(el); });
        }
    };
    Listener.prototype.reconnect = function () {
        this.socket = io.connect(this.url);
    };
    return Listener;
}());
exports.listener = new Listener("http://sampleproject.app.192.168.1.198.xip.io" + ":6001");
