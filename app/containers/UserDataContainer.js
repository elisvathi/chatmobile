import React, { Component } from 'react'
import { View, Text } from 'react-native'
import {connect} from 'react-redux'
import ActionCreators from '../actions';
import {bindActionCreators} from 'redux';
import HomePage from '../components/HomePage';
class UserDataContainer extends Component {
    state = {  }
    componentWillMount() {
       
    }
    componentDidMount() {
        this.props.fetchCurrentUser();
        // this.props.fetchConversations()
        this.props.fetchUsers();
    }
 
    render() {
        
        return (
           <HomePage/>
        );
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch);
}

export default connect((store)=>{return store}, mapDispatchToProps)(UserDataContainer);