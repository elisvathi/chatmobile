import React, { Component } from 'react'
import {Container, List, ListItem, Text} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ActionCreators from '../actions';
class UsersContainer extends Component {
    state = {  }
    componentWillMount() {
        this.props.fetchUsers();
    }
    renderList(){
        if(!this.props.users.fetched){
            if(this.props.users.fetching){
            return <Text>Loading....</Text>}else{
                return <Text>Error loading users</Text>
            }
        }else{
            return <List>
                {this.props.users.data.map((x)=>{
                    return this.renderListItem(x);
                })}
            </List>
        }
    }
    renderListItem(user){
        return <ListItem key = {user.id}>
            <Text>{user.name}</Text>
        </ListItem>
    }
    render() {
        return (
            <Container>
               {this.renderList()}
            </Container>
        );
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
    (store)=>{return {users: store.users, user: store.currentUser.data}}, mapDispatchToProps
)(UsersContainer);