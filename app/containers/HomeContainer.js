// import { View , Text} from 'react-native'
import React, { Component } from 'react'
import Expo, {Asset, AppLoading} from 'expo';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ActionCreators from '../actions';
import { View, Text } from 'react-native'
import HomePage from '../components/HomePage'
import * as AuthServices from '../lib/AuthService';

// import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Form, Input, Label, Item , Tab, Tabs, ScrollableTab} from 'native-base';
import Login from '../components/Login'
import UserDataContainer from './UserDataContainer';


class HomeContainer extends Component {
    async componentWillMount() {
        await Expo.Font.loadAsync({
          'Roboto': require('native-base/Fonts/Roboto.ttf'),
          'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        });
        this.setState({fontsLoaded: true})
      }
      state = {fontsLoaded: false}
    checkLogIn(){
        if(this.props.auth){
           
            return <UserDataContainer/>
        }else{
            return <Login loginAction = {this.props.authorize} register={this.props.register}/>
        }
    }
    render() {

        if(!this.state.fontsLoaded){
            return <AppLoading/>;
        }
       
        return (
            // <Login loginAction = {this.props.tryLogIn}/>
            this.checkLogIn()
            
        );
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
    (store)=>{return {auth: store.logInState.isLoggedIn, user: store.currentUser}}, mapDispatchToProps
)(HomeContainer);

Expo.registerRootComponent(HomeContainer);