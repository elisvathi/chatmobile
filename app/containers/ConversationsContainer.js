import React, { Component } from 'react';
import { View } from 'react-native';
import {Container, Text, List, ListItem, Fab, Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ActionCreators from '../actions';
import {listener} from '../lib/eventListener'


class ConversationsContainer extends Component {
    state = {  }
    componentWillMount() {
        this.props.fetchConversations();
        listener.listen('NewMessage', (a)=>{
           let message =  a.message;
           this.props.addMessage(message);
           this.props.updateLastMessage(message);
        }).listen('NewConversation', (a)=>{
            // let conversation = a.conversation;
            this.props.fetchConversations();

        })
    }
    generateTitle(conversation){
       
        var lastMessage = conversation.lastMessage;
        var users = conversation.users;
       
        return {
            title: this.getUsersTitle(users),
            message: lastMessage.message
        }
    }
    getUsersTitle(users){
        str = "You";
       
        users.map(
            (x)=>{
                
             if(x.id!= this.props.user.id){
                str+= ' and ' + x.name;
             }
            }
        )
        return str;
    }
    renderFullConversation(conversation){
        const {navigate}  = this.props.navigation;
        navigate('Chat',{
            
            conversation: conversation,
            
        })
    }
    renderList(){
        if(!this.props.conversations.fetched){
            if(this.props.conversations.fetching){
                return <Text>Loading...</Text>
            }else{
                return <Text>Error loading...</Text>
            }
        }else{
            return <List>
                {this.props.conversations.data.map(
                    (x)=>{
                        return this.renderItem(x);
                    }
                )}
            </List>
        }
    }
    renderItem(conversation){
        let title = this.generateTitle(conversation);
       
        return <ListItem key = {conversation.conversation.id} onPress={()=>{this.renderFullConversation(conversation)}}>
            <Text>{title.title}</Text>
            <Text>{title.message}</Text>
        </ListItem>
    }
    render() {
        
        return (
            <Container>
                {this.renderList()}
                <Fab onPress = {()=>{this.props.navigation.navigate('CreateConversation')}}>
                <Icon name="share" /></Fab>
            </Container>
        );
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}
export default connect(
    (store)=>{return {conversations: store.conversations, user: store.currentUser.data, messages: store.messages}}, mapDispatchToProps
)(ConversationsContainer);