import * as reducerHelpers from '../lib/createReducers'
import * as types from '../actions/types';
import {listener} from '../lib/eventListener';
var t = types.createResourceType(types.currentuserSimpleType).get.success;

export const dummuSubscriberReducer = reducerHelpers.createReducer({},
{[t](state, action){
    let usr = action.payload.data;
    // listener.initialize();
    listener.private('App.User.' + usr.id);
    return state;
}})