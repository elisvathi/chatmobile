import {combineReducers} from 'redux';
import * as reducers from './restReducers';
import * as authRed from './userReducer';
import * as eventsRed from './eventsReducers'
export default combineReducers(Object.assign(reducers, authRed, eventsRed));