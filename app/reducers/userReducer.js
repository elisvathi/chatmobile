import * as types from '../actions/types';
import * as helpers from '../lib/createReducers';

export const userAuthData = helpers.promiseReducer(types.logInTypes.default);
export const logInState = helpers.createReducer({isLoggedIn: false},{
    [types.LogInTrue](state, action){
        return {...state, isLoggedIn:true}
    },
    [types.LogInFalse](state, action){
        return {...state, isLoggedIn: false}
    }
})