import * as reducerHelpers from '../lib/createReducers'
import * as types from '../actions/types';
export  const conversations =  reducerHelpers.promiseReducer(types.conversationsSimpleType);

export const messages = reducerHelpers.promiseIndexedReducer(types.messagesSimpleType);

export const users = reducerHelpers.promiseReducer(types.usersSimpleTime);

export const currentUser = reducerHelpers.promiseReducer(types.currentuserSimpleType);