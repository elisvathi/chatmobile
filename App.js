import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Provider} from 'react-redux';
import HomeContainer from './app/containers/HomeContainer';
import store from './app/store';
import {StackRouter, TabRouter} from 'react-navigation';
// import {listener} from './app/lib/eventListener'

export default class App extends React.Component {

  componentDidMount() {
      
   
      
    console.disableYellowBox = true;
     

  }
  componentWillUnmount() {
    listener.usubscribeAll();
  }

  render() {
    return (
      <Provider store = {store}>
        <HomeContainer/>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
